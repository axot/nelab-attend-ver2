<?php 

function tweetmsg($msg)
{
	if(!isset($msg)) return 0;
	require_once(dirname(__FILE__) . "/twiinfo.php");
	require_once(dirname(__FILE__) . "/twitteroauth.php");

	$to = new TwitterOAuth(
		$consumer_key,
		$consumer_secret,
		$access_token,
		$access_token_secret);
	$req = $to->OAuthRequest(
		"https://api.twitter.com/1.1/statuses/update.json","POST",
			array("status"=>$msg)
	);
	return 1;
}
